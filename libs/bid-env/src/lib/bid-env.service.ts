import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class BidEnvService {
  constructor(private configService: ConfigService) {}

  nodeEnv(): string {
    const newLocal = 'NODE_ENV';
    return this.configService.get<string>(newLocal) || 'local';
  }

  get myApp(): string {
    return this.configService.get<string>('MY_APP') || 'my-app';
  }
}
