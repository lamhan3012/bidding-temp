import { DynamicModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BidEnvService } from './bid-env.service';

export class BidEnvModule {
  /**
   *
   * @param path string path to .env file (default: .env)
   * @returns DynamicModule BidEnvModule
   */
  static register(path: string): DynamicModule {
    return {
      module: BidEnvModule,
      imports: [
        ConfigModule.forRoot({
          envFilePath: path || '.env',
          ignoreEnvFile:
            process.env['NODE_ENV'] === 'local' ||
            process.env['NODE_ENV'] === 'test'
              ? false
              : true,
        }),
      ],
      providers: [BidEnvService],
      exports: [BidEnvService],
    };
  }
}
