import { BaseEntity, Repository } from 'typeorm';

export class BidBaseRepository<T extends BaseEntity> extends Repository<T> {}
