import { Inject, Injectable, Logger } from '@nestjs/common';
import * as winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import { IBidLogger } from './bid-logger.interface';
import { BidEnvService } from '@bidding/bid-env';

@Injectable()
export class BidLoggerService extends Logger implements IBidLogger {
  private readonly _logger: winston.Logger;
  constructor(
    @Inject(BidEnvService)
    private readonly _env: BidEnvService
  ) {
    super(BidLoggerService.name, { timestamp: true });
    this._logger = winston.createLogger(
      this.winstonConfig(this._env.myApp, this._env.nodeEnv())
    );
    if (this._env.nodeEnv() !== 'production') {
      this._logger.debug('Logging initialized at debug level');
    }
  }

  private winstonConfig(app: string, nodeEnv: string): winston.LoggerOptions {
    return {
      transports: [
        new DailyRotateFile({
          level: 'debug',
          filename: `./apps/${app}/logs/${nodeEnv}/debug-%DATE%.log`,
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
          ),
        }),
        new DailyRotateFile({
          level: 'error',
          filename: `../apps/${app}/logs/${nodeEnv}/error-%DATE%.log`,
          datePattern: 'YYYY-MM-DD',
          zippedArchive: false,
          maxSize: '20m',
          maxFiles: '30d',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
          ),
        }),
        new winston.transports.Console({
          level: 'debug',
          handleExceptions: true,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD-MM-YYYY HH:mm:ss',
            }),
            winston.format.simple()
          ),
        }),
      ],
      exitOnError: false,
    };
  }
  info(message: string): void {
    this._logger.info(message);
  }

  override log<T>(message: string, payload?: T): void {
    this._logger.info(`LOG: ${message}==> ${JSON.stringify(payload || {})}`);
  }

  override debug<T>(message: string, payload?: T): void {
    this._logger.debug(`DEBUG: ${message}==> ${JSON.stringify(payload || {})}`);
  }

  override error<T>(message: string, trace?: T, context?: string): void {
    this._logger.error(
      `${context || ''} ${message} -> (${trace || 'trace not provided !'})`
    );
  }

  override warn(message: string): void {
    this._logger.warn(message);
  }
}
