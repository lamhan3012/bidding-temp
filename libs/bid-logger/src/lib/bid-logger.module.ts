import { BidEnvModule } from '@bidding/bid-env';
import { DynamicModule, Module } from '@nestjs/common';
import { BidLoggerService } from './bid-logger.service';

// imports: [BidEnvModule],
// providers: [BidLoggerService],
// exports: [BidLoggerService],
@Module({})
export class BidLoggerModule {
  static register(path: string): DynamicModule {
    return {
      module: BidLoggerModule,
      imports: [BidEnvModule.register(path)],
      providers: [BidLoggerService],
      exports: [BidLoggerService],
    };
  }
}
