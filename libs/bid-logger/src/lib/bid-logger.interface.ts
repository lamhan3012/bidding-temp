export interface IBidLogger {
  log<T>(message: string, payload: T): void;
}
