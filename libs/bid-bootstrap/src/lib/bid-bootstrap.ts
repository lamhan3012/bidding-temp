import { DynamicModule, INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
export class BidBootstrap<T> {
  private readonly _AppModule: T;
  private _app: INestApplication | null;
  constructor(AppModule: T) {
    this._AppModule = AppModule;
    this._app = null;
  }
  private getApp = () => {
    if (!this._app) throw new Error('App is not created');
    return this._app;
  };

  public createApp = async () => {
    this._app = await NestFactory.create(this._AppModule);
  };

  public buildSetPrefix = (prefix: string) => {
    this.getApp().setGlobalPrefix(prefix);
  };

  public getService<SV>(module: DynamicModule, service: string) {
    return this.getApp().select(module).get(service);
  }

  public async run() {
    await this.getApp().listen(3000);
  }
}
