# bid-base-service

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test bid-base-service` to execute the unit tests via [Jest](https://jestjs.io).
