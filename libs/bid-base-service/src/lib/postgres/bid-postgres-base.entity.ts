import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

export class _BidPostgresEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: Date;

  @Column({ type: 'varchar', length: 255, default: 'Admin' })
  createdBy!: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt!: Date;

  @Column({ type: 'varchar', length: 255, default: 'Admin' })
  updatedBy!: string;
}
