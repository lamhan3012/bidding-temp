import { DynamicModule, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { INJECT_NATS } from './bid-client-nats.constant';
import { BidClientNatsService } from './bid-client-nats.service';

@Module({})
export class BidClientNatsModule {
  static register(servers: string[]): DynamicModule {
    return {
      module: BidClientNatsModule,
      imports: [
        ClientsModule.register([
          {
            name: INJECT_NATS,
            transport: Transport.NATS,
            options: {
              servers: servers,
            },
          },
        ]),
      ],
      providers: [BidClientNatsService],
      exports: [BidClientNatsService],
    };
  }
}
