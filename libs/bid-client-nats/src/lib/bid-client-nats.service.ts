import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { INJECT_NATS } from './bid-client-nats.constant';

@Injectable()
export class BidClientNatsService {
  constructor(
    @Inject(INJECT_NATS)
    private readonly _client: ClientProxy
  ) {}

  async send<T, R>(pattern: { cmd: string }, data: T) {
    console.log('BidClientNatsService', pattern, data);
    return lastValueFrom<R>(this._client.send(pattern, data));
  }
}
