import { applyDecorators } from '@nestjs/common';
import { MessagePattern, Transport } from '@nestjs/microservices';

export const MessagePatternNATS = (cmd: string) => {
  return applyDecorators(MessagePattern({ cmd: cmd }, Transport.NATS));
};
