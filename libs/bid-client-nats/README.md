# bid-client-nats

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test bid-client-nats` to execute the unit tests via [Jest](https://jestjs.io).
