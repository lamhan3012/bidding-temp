/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { BidBootstrap } from '@bidding/bid-bootstrap';
import { AppModule } from './app/app.module';

async function bootstrap() {
  const bidBootstrap = new BidBootstrap(AppModule);
  await bidBootstrap.createApp();
  bidBootstrap.buildSetPrefix('api');
  await bidBootstrap.run();
}

bootstrap();
