import { Controller, Get, Inject } from '@nestjs/common';

import { AppService } from './app.service';
import { BidLoggerService } from '@bidding/bid-logger';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject(BidLoggerService)
    private readonly _logger: BidLoggerService
  ) {}

  @Get()
  getData() {
    this._logger.info(this.getData.name);
    return this.appService.getData();
  }
}
