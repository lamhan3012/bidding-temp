import { BidLoggerModule } from '@bidding/bid-logger';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GwNatsModule } from './modules';
import { BidEnvModule } from '@bidding/bid-env';

const pathENV = './apps/client-gateway/.env';
@Module({
  imports: [
    BidEnvModule.register(pathENV),
    BidLoggerModule.register(pathENV),
    GwNatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
