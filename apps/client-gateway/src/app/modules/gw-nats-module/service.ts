import { BidClientNatsService } from '@bidding/bid-client-nats';
import { Inject } from '@nestjs/common';

export class GwNatsService {
  constructor(
    @Inject(BidClientNatsService)
    public readonly _client: BidClientNatsService
  ) {}

  async send<T>(pattern: { cmd: string }, data: T) {
    return this._client.send(pattern, data);
  }
}
