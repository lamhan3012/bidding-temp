import { BidClientNatsModule } from '@bidding/bid-client-nats';
import { Global, Module } from '@nestjs/common';
import { GwNatsService } from './service';

@Global()
@Module({
  imports: [BidClientNatsModule.register(['nats://127.0.0.1:4222'])],
  providers: [GwNatsService],
  exports: [GwNatsService],
})
export class GwNatsModule {}
