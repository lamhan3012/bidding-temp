import { Inject, Injectable } from '@nestjs/common';
import { GwNatsService } from './modules';

@Injectable()
export class AppService {
  constructor(
    @Inject(GwNatsService)
    private readonly _client: GwNatsService
  ) {}
  getData() {
    return this._client.send({ cmd: 'getData' }, { message: 'Hello API' });
  }
}
