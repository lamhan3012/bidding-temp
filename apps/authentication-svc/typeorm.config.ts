import { DataSource } from 'typeorm';
import dotenv from 'dotenv/config';

dotenv.config({ path: '.env' });
const entities = [];

export default new DataSource({
  type: 'postgres',
  host: '45.32.54.94',
  port: 5432,
  username: 'postgres',
  password: '123456',
  database: 'jack',
  entities: entities,
  synchronize: true,
  migrationsTableName: 'migrations',
  migrations: [],
  ssl: false,
});
