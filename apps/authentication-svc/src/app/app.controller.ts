import { MessagePatternNATS } from '@bidding/bid-client-nats';
import { Controller } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePatternNATS('getData')
  getData(payload: any) {
    console.log(payload);
    console.log('AppController.getData');
    return this.appService.getData();
  }
}
