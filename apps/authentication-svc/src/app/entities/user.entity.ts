import { _BidPostgresEntity } from '@bidding/bid-base-service';
import { Column, Entity, OneToOne } from 'typeorm';
import { Authentication } from './auth.entity';

@Entity('Users')
export class User extends _BidPostgresEntity {
  @Column({ name: 'full_name', type: 'varchar' })
  fullName: string;

  @OneToOne(() => Authentication, { eager: true })
  auth: Authentication;
}
