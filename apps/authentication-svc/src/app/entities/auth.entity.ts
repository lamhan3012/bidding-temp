import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Authentications')
export class Authentication {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: Date;

  @Column({ type: 'varchar', length: 255, default: 'Admin' })
  createdBy!: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt!: Date;

  @Column({ type: 'varchar', length: 255, default: 'Admin' })
  updatedBy!: string;

  @Column({ name: 'user_name', type: 'varchar' })
  userName: string;

  @Column({ name: 'password', type: 'varchar' })
  password: string;

  @Column({ name: 'salt', type: 'varchar' })
  salt: string;
}
